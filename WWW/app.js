const express = require('express')
const app = express()
var mysql = require('mysql');
const moment = require('moment');
var bodyParser = require('body-parser');
var Validator = require('validatorjs');
var async = require('async')
var fs = require("fs");
var request = require('requestretry');
var http = require('http');
var jade = require('jade');
var pdf = require('html-pdf');
var errors = require('throw.js');
var GoogleAuth = require('google-auth-library');
var uuidv4 = require('uuid/v4');

var con = mysql.createConnection({
  host: "localhost",
  user: "admin",
  password: "ff1312d653e45756b87e3ceef5a774b87907be60ba7cee9e",
  database: "GoPhysio"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

app.use(bodyParser.json({limit: '50mb', type: 'application/json'}));

var jsonParser = bodyParser.json()

http.globalAgent.maxSockets = 1000;

function GetOAuthIDAndEmail(OAuthToken, callback)
{
	var sql = "SELECT PractitonerID FROM UserCookie WHERE ID = ? AND ExpiryDate > NOW()";
	var val = OAuthToken;
	con.query(sql, val, function(err, results)
	{
		if (err) return callback(new errors.BadRequest(err));

		if (results.length == 0) return callback(new errors.Forbidden());

		var pracID = results[0].PractitonerID;

		var obj = {OAuthPrimary: pracID, Email: "", IsValidAuth: true};
		callback(null, obj);
	});
}

function GetUserImagePath(id)
{
	return 'http://api.4dhealthscience.com:4000/UserImages/' + id + ".jpeg";
}

app.all('*', function(req, res, next) 
{
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE'); 
	res.header('Access-Control-Allow-Headers', 'Content-Type');

	if (req.method === 'OPTIONS') 
	{
      	res.sendStatus(200);
      	res.end();
	} 
	else 
	{
	//...other requests
		return next();
	}
});

function clientErrorHandler (err, req, res, next) 
{
	if (err)
	{
		console.log("error handler");
		res.status(err.statusCode || 500).json(err);
	}
}



app.param('OAuthToken', function(req, res, next, oAuthToken)
{
	//Call google to convert OAuthToken to id, see if it's real.
	GetOAuthIDAndEmail(oAuthToken, function(err, oAuth)
	{
		if (err) return next(err);
		//Check DB To see if user is a practitioner.
		var sql = "SELECT COUNT(*) as Count FROM PractitionerRecords WHERE OAuthPrimary = ?";
		var val = oAuth.OAuthPrimary;
		con.query(sql, val, function(err, results)
		{
			//return OAuth ID or 400 error.
			if (results[0].Count == 1)
			{
				req.OAuthPrimary = oAuth.OAuthPrimary; 
				return next();
			}
			else
			{
				console.log('prac not found');
				return next(new errors.Forbidden());
			}
		});
	});
})

app.param('PatientID', function (req, res, next, patientID )
{
	//Check DB to see that user is in there.
	var sql = "SELECT COUNT(*) as Count FROM PatientRecords WHERE PatientID = ?";
	var val = patientID;
	con.query(sql, val, function(err, results)
	{
		//return OAuth ID or 400 error.
		if (results[0].Count == 1)
		{
			return next();
		}
		else
		{
			console.log('patient not found');
			return next(new errors.NotFound());
		}
	});
})


app.param('PainReportID', function (req, res, next, painReportID )
{
	//Check DB to see that user is in there.
	var sql = "SELECT COUNT(*) as Count FROM PainReports WHERE PainReports.ID = ?";
	var val = painReportID;
	con.query(sql, val, function(err, results)
	{
		//return OAuth ID or 400 error.
		if (results[0].Count == 1)
		{
			return next();
		}
		else
		{
			console.log('Pain Report not found');
			return next(new errors.NotFound());
		}
	});
})

app.param('SessionID', function (req, res, next, sessionID )
{
	//Check DB to see that user is in there.
	var sql = "SELECT COUNT(*) as Count FROM Session WHERE ID = ?";
	var val = sessionID;
	con.query(sql, val, function(err, results)
	{
		//return OAuth ID or 400 error.
		if (results[0].Count == 1)
		{
			return next();
		}
		else
		{
			console.log('Session not found');
			return next(new errors.NotFound());
		}
	});
})

//Add data to database (For Now geneate mock data for email address(with a mock function), as well mock OAuthID)
const createAccountRules = 
{
	TokenID: 'required',
	FirstName: 'required',
	LastName: 'required',
	ClinicName: 'required',
	ClinicLocation: 'required',
	ClinicPhone: 'required',
	APIKey: 'required'
};

app.post('/createAccount', jsonParser, function (req, res, next) 
{
	var validation = new Validator(req.body, createAccountRules);

	//data validation
	if (validation.fails())
	{
		return next(new errors.BadRequest(validation.errors.all()));
	}

	
	async.waterfall(
	[
		function(callback)
		{
			//Verify Token via google api.
			var CLIENT_ID = '518415728508-ck5dn6a76i5k1fr5dvtfc9ucigoc3crf.apps.googleusercontent.com';
			var auth = new GoogleAuth;
			var client = new auth.OAuth2(CLIENT_ID, '', '');

			client.verifyIdToken(req.body.TokenID, CLIENT_ID, function(e, login) 
			{
				if (e) return callback(new errors.BadRequest(e));

				var payload = login.getPayload();
				var userid = payload['sub'];
				console.log(userid);
				return callback(null, userid);
			});
		},
		function(userID, callback)
		{
			//Check that the google id isn't already a user.
			var sql = "SELECT COUNT(*) as Count FROM PractitionerRecords WHERE OAuthPrimary = ?";
			var val = userID;
			con.query(sql, val, function(err, results)
			{
				if(err) return callback(new errors.BadRequest(err));

				if (results[0].Count > 0)
				{
					return callback(new errors.BadRequest());
				}
				return callback(null, userID);
			});
		},
		function(userID, callback)
		{
			//Insert User into database. 
			var sql = "INSERT INTO PractitionerRecords SET ?";
			var val = 
			{
				OAuthPrimary: userID,
				Email: "",
				FirstName: req.body.FirstName,
				LastName: req.body.LastName,
				ClinicName: req.body.ClinicName,
				ClinicLocation: req.body.ClinicLocation,
				ClinicPhone: req.body.ClinicPhone,
				APIKey: req.body.APIKey
			}
			con.query(sql, val, function(err, result)
			{
				if (err) return callback(new errors.BadRequest(err));

				return callback(null, null);
			});
		}
	],
	function (err, result)
	{
		if (err)
		{
			console.log("error");
			return next(err);
		}
		return res.status(200).json(null);
	});
})

app.post('/login', jsonParser, function(req, res, next)
{
	//Validate input.
	var validationRules =
	{
		TokenID: 'required'
	}

	//Validate token via google api.
	var validation = new Validator(req.body, validationRules);

	if (validation.fails())
	{
		return next(new errors.BadRequest(validation.errors.all()));
	}

    async.waterfall(
    [
    	function(callback)
    	{
    		//Validate user is a valid user by seeing if ID from google matches any practitioner record's ID.
    		var CLIENT_ID = '518415728508-ck5dn6a76i5k1fr5dvtfc9ucigoc3crf.apps.googleusercontent.com';
    		var auth = new GoogleAuth;
    		var client = new auth.OAuth2(CLIENT_ID, '', '');

    		client.verifyIdToken(req.body.TokenID, CLIENT_ID, function(e, login) 
    		{
    			if (e) return callback(new errors.NotFound(e));

    			var payload = login.getPayload();
    			var userid = payload['sub'];
    			console.log(userid);
    			return callback(null, userid);
    		});
    	},
    	function(userID, callback)
    	{
    		//Check that user is a practitioner in the database(has created account with this google linked).
    		var sql = "SELECT COUNT(*) as Count FROM PractitionerRecords WHERE OAuthPrimary = ?";
    		var val = userID;
    		con.query(sql, val, function(err, results)
    		{
    			if(err) return callback(new errors.BadRequest(err));

    			if (results[0].Count == 0)
    			{
    				return callback(new errors.NotFound());
    			}
    			return callback(null, userID);
    		});
    	},
    	function(userID, callback)
    	{
    		//Create Moment date for expiry.
    		var expiryDate = moment().add(3, 'days');

    		//Generate UUID
    		var uuid = uuidv4();

    		uuid = uuid.replace('-', '');

    		var val = [uuid, userID, expiryDate.format('YYYY-MM-DD HH:mm:ss')];

    		var sql = "INSERT INTO UserCookie (ID, PractitonerID, ExpiryDate) VALUES(?, ?, ?)";
    		con.query(sql, val, function(err, result)
    		{
    			if (err) return callback(new errors.BadRequest(err));
    			//Send UUID and expiry date back to client.
    			var cookieData = {UUID: uuid, ExpiryDate: expiryDate.get()};

    			return callback(null, cookieData);
    		});
    	}
    ],
    function (err, cookieData)
    {
    	if (err)
    	{
    		console.log("error");
    		return next(err);
    	}

    	return res.status(200).json(cookieData);
    });
})

const addPatientRules = 
{
	FirstName: 'required',
	LastName: 'required',
	Gender: 'required',
	Address: 'required',
	Phone: 'required',
	Email: 'required|email',
	BirthDate: 'required|date',
	Referal: 'required',
	ConditionReport: 'required',
	Symptoms: 'required',
	Goals: 'required',
	Exercises: 'required'
};

app.post('/addNewPatient/:OAuthToken', jsonParser, function (req, res, next) 
{
	var validation = new Validator(req.body, addPatientRules);

	//data validation
	if (validation.fails())
	 {
	 	return next(errors.BadRequest(validation.errors.all()));
	 }


	var date = moment(req.body.BirthDate).format('YYYY-MM-DD HH:mm:ss');
	var post = {PractitionerID: req.OAuthPrimary, FirstName: req.body.FirstName, LastName: req.body.LastName,
	 Gender: req.body.Gender, Address: req.body.Address, Phone: req.body.Phone, 
	 Email: req.body.Email, BirthDate: date, Referal:req.body.Referal, 
	 PatientCondition: req.body.ConditionReport, Symptoms: JSON.stringify(req.body.Symptoms)};

	//Insert Data.
	var sql = "INSERT INTO PatientRecords SET ?";
	con.query(sql, post, function (err, result) 
	{
    	if (err)
    	{
    		return next(err);
    	}
    	else
    	{
    		res.status(200).json({PatientID: result.insertId});
    	}
    	//Get Patient ID(from Result?)
    	var patientID = result.insertId;

    	//Add image as a jpg to a folder, with name=patient_id+datetimenow
    	var folderPath = '/root/WebMedia/UserImages/';
    	var imageName = patientID + '.jpeg';
    	var imagePath = folderPath + imageName;
    	//var buff = new Buffer(req.body.UserImage, 'base64');
    	fs.writeFile(imagePath, req.body.UserImage, 'base64', function (err)
    	{
    		if (err) return console.log(err);
    	});
    	
    	//Insert Goals.
  		var goalValues = req.body.Goals.map(function(item)
  		{
  			//return {Description: item.Description, PatientID: patientID};
  			return [item.Description, patientID];
  		});
  		var goalsSql = "INSERT INTO Goals (Description, PatientID) VALUES ?";
  		con.query(goalsSql, [goalValues], function(err, result)
  		{
  			if (err) return console.log(err);
  		});

  		//Insert Exercises
	  	var exerciseValues = req.body.Exercises.map(function(item)
	  	{
	  		//return {ExerciseID: item.ID, PatientID: patientID, TargetReps:item.TargetReps, Accuracy: item.TargetAccuracy};
	  		return [item.ID, patientID, item.TargetReps, item.TargetAccuracy];
	  	})
	  	var exerciseSql = "INSERT INTO ActiveExercises (ExerciseID, PatientID, TargetReps, Accuracy) VALUES ?";
	  	con.query(exerciseSql, [exerciseValues], function(err, result)
	  	{
	  		if (err) return console.log(err);
	  	});

	  	//Get API Key
	  	var sql = "SELECT APIKey FROM PractitionerRecords WHERE OAuthPrimary = ?"
		var val = req.OAuthPrimary
		con.query(sql, val, function(err, results)
		{
			if (err) return console.log(err);
			var apiKey = results[0].APIKey;
			var url = "http://devs.patient-inquiry.com/patient2/json/?Api-Key=" + apiKey;
		  	//Add Patient To FOTO.
		  	var date = "\/Date({0}-0400)\/";
		  	date = date.replace("{0}", new Date(req.body.BirthDate).getTime());
		  	/*var data = 
		  	{
			  	FirstName: req.body.FirstName, 
		  		LastName: req.body.LastName, 
		  		Alias: req.body.FirstName + patientID, 
		  		DateOfBirth: "\/Date(928164000000-0400)\/",
		  		Email: req.body.Email, 
		  		ExternalId: patientID.toString(),
		  		ExternalSiteId: "",
		  		Gender: (req.body.Gender == 1) ? 'M' : 'F',
		  		Language: "en",
		  		MiddleIntial: "A"
	  		};*/
	  		var data = 
			{
			  FirstName: req.body.FirstName,
			  LastName: req.body.LastName,
			  MiddleInitial:"",
			  DateOfBirth: date,
			  Email: req.body.Email,
			  ExternalId: patientID,
			  ExternalSiteId:"",
			  Alias:req.body.FirstName + patientID,
			  Gender: (req.body.Gender == 1) ? 'M' : 'F',
			  Language:"en"
	  		}
	  		var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};


		  	request({url:url, method:'POST', json: [data], headers: headers, proxy: "http://127.0.0.1:8888"}, function (error, response, body) 
			{
				if (error)
				{
					console.log(error);
				}
			});
		});	  	
  	});
})

app.get('/addpatient/exercisesList/:OAuthToken', function (req, res, next) 
{
	//Get all exercises from a query.
	var sql = "SELECT ID, Title, Description, ImageURL, VideoURL FROM Exercises";
	con.query(sql, function (err, results) 
	{
		if (err) return next(err);
		 //Return them as a JSON object. 
		 res.status(200).json(results);
	});
})

app.get ('/unackPainReports/:OAuthToken', function (req, res, next)
{
	//Get A list the list of patients from the practitoner, then get all the patient pain reports that are not marked as aknowlaged.
	var sql = "SELECT PainReports.JointIndex, FirstName, LastName FROM (SELECT PatientID, FirstName, LastName FROM PatientRecords WHERE PractitionerID = ?) AS t1 JOIN PainReports ON PainReports.PatientID = t1.PatientID AND PainReports.Acknowledged = FALSE";
	con.query (sql, req.OAuthPrimary, function (err, result)
	{
		if (err) next(err);

		return res.json(result);
	});
})

app.get('/dashboard/graphdata/:OAuthToken', function(req, res, next)
{
	//Return a JSON object that contains each month and a value.

	//Query database for a list of dates for active patients. Select number of entries before x dates, select number before x month + 1 minus them. 
	var sql = "SELECT AddedDate FROM PatientRecords WHERE PractitionerID = ? ORDER BY AddedDate";
	con.query(sql, req.OAuthPrimary, function(err, results)
	{
		if (err) next(err);
		//Make an empty array containing 0's of len 12
		var dateValues = Array.apply(null, Array(12)).map(Number.prototype.valueOf,0);

		//Date Equal to end of this current month.
		var startDate = moment().endOf('month');

		for (i  = 0; i < 11; i++)
		{
			dateValues[i] = results.filter(function (item)
			{
				return (item.AddedDate < (moment(startDate).subtract(i, 'months')) && item.AddedDate > (moment(startDate).subtract((i + 1), 'months') ));
			}).length;
		}
		return res.json(dateValues);
	});
})

app.get('/dashboard/tabs/:OAuthToken', function (req, res, next)
{
	async.parallel
	(
		[
			function (callback)
			{
				//Get All Outstanding Pain Reports
				var sql = 'SELECT * FROM OutstandingPainReportView WHERE PractitionerID = ?';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Has " + obj.OutstandingPainReportCount + " Unacknowlaged Pain Reports";
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'red', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

			//Get All Patients Not Participated in X number of days where X = set by user.
			function (callback)
			{
				var sql = 'SELECT PatientID, FirstName, LastName, UserImageURI, TIMESTAMPDIFF(DAY, PatientRecordsView.LastParticipationDate, NOW()) as NoPartDays FROM PatientRecordsView WHERE PractitionerID = ? HAVING NoPartDays >= ?';
				var val = [req.OAuthPrimary, 2]
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Has Not Participated In " + obj.NoPartDays + " days";
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'red', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

			//Get All Requested Users that have outstanding requested appointments.
			function (callback)
			{
				var sql = 'SELECT RequestedAppointments.ID, RequestedAppointments.AppointmentDate, PatientRecords.FirstName, PatientRecords.LastName, PatientRecords.UserImageURI, PatientRecords.PatientID FROM RequestedAppointments JOIN PatientRecords ON RequestedAppointments.PatientID = PatientRecords.PatientID WHERE PatientRecords.PractitionerID = ? AND RequestedAppointments.Completed = FALSE';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Has Outstanding Appointment Request For " + obj.AppointmentDate;
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'green', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

			//Get users awaitng approval.
			function (callback)
			{
				var sql = 'SELECT PatientID, FirstName, LastName, UserImageURI FROM PatientRecords WHERE PractitionerID = ? AND IsApproved = FALSE';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Awaiting Approval"
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'yellow', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

		
			//Get All users who have started the program in the last y number of days, where Y is set by the user. 
			function (callback)
			{
				var sql = 'SELECT PatientID, FirstName, LastName, UserImageURI, AddedDate FROM PatientRecords WHERE PractitionerID = ? AND AddedDate > DATE_SUB(NOW(), INTERVAL 2 DAY)';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Started Program";
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'green', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

			//Get All users who have completed the program in the last y number of days.
			function (callback)
			{
				var sql = 'SELECT PatientID, FirstName, LastName, UserImageURI, CompletionDate FROM PatientRecords WHERE PractitionerID = ? AND PatientRecords.CompletionDate > DATE_SUB(NOW(), INTERVAL 2 DAY)';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Completed Program";
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'green', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			},

			//Get All users who have reached x months of participation, in the last y number of days.
			function (callback)
			{
				var sql = 'SELECT PatientID, FirstName, LastName, UserImageURI, AddedDate, TIMESTAMPDIFF(DAY, AddedDate, NOW()) AS DaysSinceStarted FROM PatientRecords WHERE PractitionerID = ? HAVING DaysSinceStarted >= 30 AND(DaysSinceStarted % 30) >= 0 AND (DaysSinceStarted % 30) <= 2';
				var val = req.OAuthPrimary;
				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var ret = results.map(function (obj)
					{
						var messageBody = "Patient Reached " + 30.0*Math.round(obj.DaysSinceStarted/30.0) + " Days Of Participation";
						return {PatientName: obj.FirstName + " " + obj.LastName, PatientID: obj.PatientID, MessageBody: messageBody, MessageType: 'green', UserImageURI: GetUserImagePath(obj.PatientID)};
					});
					return callback(null, ret);
				});
			}
		],
		function (err, results)
		{
			if (err) return next(err);
			var merged = [].concat.apply([], results);
			//Emalgermate this into a list of Event : {PatientName=, PatientID, MessageBody, MessageType};

			res.status(200).json(merged);
		}
	)
})

app.get('/patientReport/:OAuthToken/:PatientID', function(req, res, next)
{
	//Get List of exercises, reps, completed reps, accuracy data, accuracy, painreports count for this active exercise.
	async.parallel
	(
		[
			function(callback)
			{
				var sql = "SELECT ActiveExerciseView.ID, ActiveExerciseView.TargetReps, ActiveExerciseView.CompletedReps, ActiveExerciseView.Accuracy, ActiveExerciseView.AverageAccuracy, Exercises.Title, (SELECT COUNT(PainReports.ID) from PainReports WHERE PainReports.ActiveExerciseID = ActiveExerciseView.ID) as PainReportCount FROM ActiveExerciseView JOIN Exercises ON ActiveExerciseView.ExerciseID = Exercises.ID WHERE ActiveExerciseView.PatientID = ?";
				var val = req.params.PatientID;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					//if (err) res.status(420).send("SQL Error");
					return callback(null, {Key: "ExercisesOverviews", Value: results});
					//res.status(200).json(results);

				});
			},
			function (callback)
			{
				//var sql = "SELECT dates.fulldate, (SELECT COUNT(*) FROM ActiveExerciseView WHERE ActiveExerciseView.PatientID = ? AND dates.fulldate >= ActiveExerciseView.StartDate AND dates.fulldate <= ActiveExerciseView.CompletedDate) AS ActiveCount, (SELECT COUNT(*) FROM ActiveExerciseView WHERE ActiveExerciseView.PatientID = ? AND cast(ActiveExerciseView.CompletedDate as date) = dates.fulldate) as CompletedCount, (SELECT COUNT(*) FROM PainReports WHERE PainReports.PatientID = ? AND cast(PainReports.ReportedDate as date) = dates.fulldate) as PainReportCount FROM dates WHERE dates.fulldate <= NOW() AND dates.fulldate > DATE_ADD(CURDATE(), INTERVAL -30 DAY) ORDER BY dates.fulldate DESC";
				var sql = "SELECT SessionReportView.ID, SessionReportView.DateOfSession, SessionReportView.RepCount, SessionReportView.OutstandingRepCount, (SessionReportView.RepCount / SessionReportView.OutstandingRepCount) as ProgressPercentage, SessionReportView.PainReportCount FROM SessionReportView WHERE PatientID = ? AND SessionReportView.DateOfSession > DATE_SUB(NOW(), INTERVAL 30 DAY) ORDER BY SessionReportView.DateOfSession DESC";
				var val = [req.params.PatientID];
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					//if (err) res.status(420).send("SQL Error");
					return callback(null, {Key: "Sessions", Value: results});
				});
			}
		],
		function(err, results)
		{
			if (err) 
			{
				return next(err);
			}

			 var obj = {};
			 results.forEach(function(item)
			 {
			 	obj[item.Key] = item.Value;
			 });

			 return res.status(200).json(obj);
		}
	);
})

app.get('/patientDetails/:OAuthToken/:PatientID', function(req, res, next)
{
	//Get PatientName, Age, Gender
	var sql = "SELECT FirstName, LastName, Birthdate, Gender, UserImageURI FROM PatientRecords WHERE PatientID = ?";
	var val = req.params.PatientID;
	con.query(sql, val, function(err, result)
	{
		if (err) 
		{
			return next(err);
		}
		result[0].UserImageURI = GetUserImagePath(req.params.PatientID);
		return res.status(200).json(result[0]);
	});
})

app.get('/inbox/search/:OAuthToken/:SearchString', function(req, res, next)
{
	var sql = (req.params.SearchString == '*') ? "SELECT PatientID, FirstName, LastName, UserImageURI FROM PatientRecords WHERE PractitionerID = ?" 
	: "SELECT PatientID, FirstName, UserImageURI, LastName FROM PatientRecords WHERE PractitionerID = ? AND (FirstName LIKE ? OR LastName LIKE ?)";
	var val = [req.OAuthPrimary, '%' + req.params.SearchString + '%', '%' + req.params.SearchString + '%'];
	con.query(sql, val, function (err, results)
	{
		if (err) 
		{
			return next(err);
		}
		results.forEach(function(item)
		{
			item.UserImageURI = GetUserImagePath(item.PatientID);
		});
		return res.status(200).json(results);
	});
})

app.get('/inbox/searchByPainReport/:OAuthToken/:SearchString', function(req, res, next)
{
	var sql = (req.params.SearchString == '*') ? "SELECT PatientID, FirstName, LastName, UserImageURI, OutstandingPainReportCount FROM OutstandingPainReportView WHERE PractitionerID = ?" 
	: "SELECT PatientID, FirstName, UserImageURI, LastName, OutstandingPainReportCount FROM OutstandingPainReportView WHERE PractitionerID = ? AND (FirstName LIKE ? OR LastName LIKE ?) AND OutstandingPainReportCount > 0";
	var val = [req.OAuthPrimary, '%' + req.params.SearchString + '%', '%' + req.params.SearchString + '%'];
	con.query(sql, val, function (err, results, next)
	{
		if (err) 
		{
			return next(err);
		}
		results.forEach(function(item)
		{
			item.UserImageURI = GetUserImagePath(item.PatientID);
		});
		return res.status(200).json(results);
	});
})

app.get('/inbox/patientTabInfo/:OAuthToken/:PatientID', function (req, res, next)
{
	async.parallel
	(
		[
			function (callback)
			{
				var sql = "SELECT ActiveExerciseView.TargetReps, Exercises.Title FROM ActiveExerciseView JOIN Exercises ON ActiveExerciseView.ExerciseID = Exercises.ID WHERE ActiveExerciseView.PatientID = ?"
				var val = req.params.PatientID;

				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					return callback(null, {Key: "Exercises", Value: results});
				});
			},

			function (callback)
			{
				var sql = "SELECT PainReports.JointPainData, PainReports.ReportedDate, Exercises.Title FROM PainReports JOIN ActiveExerciseView ON ActiveExerciseView.ID = PainReports.ActiveExerciseID JOIN Exercises ON Exercises.ID = ActiveExerciseView.ExerciseID WHERE PainReports.PatientID = ? AND PainReports.Acknowledged = FALSE ORDER BY PainReports.ReportedDate LIMIT 1";
				var val = req.params.PatientID;

				con.query(sql, val, function (err, results)
				{
					if (err)
					{
						return callback(err, null);
					}
					//if (results.length == 0) return callback(new errors.NotFound(), null);


					return callback(null, {Key: "LastPainReport", Value: results[0]});
				});
			},
		], 
		function (err, results)
		{
			if (err) 
			{
				console.log(err);
				return next(err);
			}

			 var obj = {};
			 results.forEach(function(item)
			 {
			 	obj[item.Key] = item.Value;
			 });
			 return res.status(200).json(obj);
		}
	);
})

app.get('/navigation/details/:OAuthToken', function (req, res, next)
{
	var sql = "Select FirstName, LastName FROM PractitionerRecords WHERE OAuthPrimary = ?";
	var val = req.OAuthPrimary;

	con.query(sql, val, function (err, results)
	{
		if (err) return next(err);

		return res.status(200).json({Name:results[0].FirstName + ' ' + results[0].LastName});
	});
})

app.get('/programSetup/activeExercises/:OAuthToken/:PatientID', function(req, res, next)
{
	var sql = "SELECT Exercises.ID as ExerciseID, Exercises.VideoURL, Exercises.ImageURL, Exercises.Title, Exercises.Description, t1.TargetReps, t1.Accuracy, t1.ID as ActiveExerciseID FROM Exercises LEFT JOIN (SELECT ExerciseID, TargetReps, Accuracy, ID FROM ActiveExerciseView where PatientID = ?) as t1 ON Exercises.ID = t1.ExerciseID";
	var val = req.params.PatientID;
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		return res.status(200).json({Exercises: results});
	});
})

app.post('/programSetup/alterExercises/:OAuthToken/:PatientID', jsonParser, function(req, res, next)
{
	async.parallel
	(
		[
			function (callback)
			{
				var sql = "INSERT INTO ActiveExercises (ID, ExerciseID, PatientID, TargetReps, Accuracy) VALUES ? ON DUPLICATE KEY UPDATE TargetReps=VALUES(TargetReps), Accuracy=VALUES(Accuracy)"; 

				var vals = req.body.Exercises.map(function(item)
				{
					item.ActiveExerciseID = (item.ActiveExerciseID) || null;
					return [item.ActiveExerciseID, item.ExerciseID, req.params.PatientID, item.TargetReps, item.TargetAccuracy];
				});

				if (vals.length > 0)
				{
					con.query(sql, [vals], function(err, results)
					{
						if (err)
						{
							console.log(err);
							return callback(err, null);
						}
						return callback(err, results);
					});
				}
				else
				{
					return callback(null, null);
				}
				
			},
			function (callback)
			{
				var sql = "DELETE FROM ActiveExercises WHERE ID IN (?)";
				var vals = req.body.ExercisesToDelete;
				if (vals.length > 0)
				{
					var query = con.query(sql, [vals], function(err, results)
					{
						if (err)
						{
							console.log(err);
							return callback(err, null);
						}
						return callback(err, results);
					});
					//console.log(query.sql);
				}
				else
				{
					return callback(null, null);
				}
			}
		],
		function (err, results)
		{
			if (err) 
			{
				return next(err);
			}
			return res.status(200).json({Message: "Success"});
		}
	);
})

app.get('/painReport/recentReports/:OAuthToken/:PatientID', function(req, res, next)
{
	var sql = "SELECT ID as PainReportID, ReportedDate FROM PainReports WHERE PatientID = ? AND Acknowledged = FALSE ORDER BY ReportedDate DESC";
	var val = req.params.PatientID;
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		return res.status(200).json({PainReports: results});
	});
})

app.get('/painReport/painReportDetails/:OAuthToken/:PainReportID', function(req, res, next)
{
	var sql = "SELECT JointPainData FROM PainReports WHERE ID = ?";
	var val = req.params.PainReportID;
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		results[0].VideoURL = "http://api.4dhealthscience.com:4000/PainVideo/" + req.params.PainReportID + ".mp4";
		return res.status(200).json({PainReportDetails: results[0]});
	});
});

app.post('/painReport/markAsResolved/:OAuthToken/:PainReportID', jsonParser, function(req, res, next)
{
	var sql = "UPDATE PainReports SET Acknowledged=TRUE WHERE ID=?"; 
	var val = req.params.PainReportID;
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		return res.status(200).json({Message: "Success"});
	});
})

app.get('/patientReport/exerciseStats/:OAuthToken/:PatientID', function(req, res, next)
{
	var sql = "SELECT (SELECT COUNT(*) FROM ActiveExerciseView WHERE PatientID = ?) AS Total, (SELECT COUNT(*) FROM ActiveExerciseView WHERE PatientID = ? AND CompletedReps >= TargetReps) AS Completed, (SELECT COUNT(*) FROM PainReports WHERE PainReports.PatientID = ? AND PainReports.Acknowledged = FALSE) as PainReportCount";
	var val = [req.params.PatientID, req.params.PatientID, req.params.PatientID];
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		if (results.length == 0) return next(new errors.NotFound());

		return res.status(200).json({ExerciseStats: results[0]});
	});
});

app.get('/sessionReport/sessionData/:OAuthToken/:SessionID', function(req, res, next)
{
	var sql = "SELECT DateOfSession, RepCount, PainReportCount, OutstandingRepCount FROM SessionReportView WHERE ID = ?";
	var val = req.params.SessionID;
	con.query(sql, val, function(err, results)
	{
		if (err) 
		{
			return next(err);
		}
		if (results.length == 0) return next(new errors.NotFound());

		results[0].ImageURL = "http://api.4dhealthscience.com:4000/SessionImages/" + req.params.SessionID + ".jpeg";
		return res.status(200).json({SessionData: results[0]});
	});
})

app.get('/discharge/dischargePatient/:OAuthToken/:PatientID/:Visits', function(req, res, next)
{
	console.log('discharge started');
	//req.connection.setTimeout( 1000 * 60);
	
	var epID;
	async.waterfall
	(
		[
			function(callback)
			{
				var sql = "SELECT PatientRecords.InDischargeProcess FROM PatientRecords WHERE PatientRecords.PatientID = ?"
				var val = req.params.PatientID;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					if (results[0].InDischargeProcess == true)
					{
						//return callback(new errors.BadRequest(), null);
					}
					else
					{
						return callback(null);
					}
					
				});
			},
			function (callback)
			{
				var sql = "UPDATE PatientRecords SET InDischargeProcess = TRUE WHERE PatientID = ?"
				var val = req.params.PatientID;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}

					return callback(null);
				});
			},
			function(callback)
			{
				//Get Clinican API Key from db.
				var sql = "SELECT APIKey FROM PractitionerRecords WHERE OAuthPrimary = ?"
				var val = req.OAuthPrimary;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					console.log('Got API Key');
					return callback(null, results[0].APIKey);
				});
			},
			function(apiKey, callback)
			{
				//Get Episode from FOTO(using our patient ID)
				var url = "http://devs.patient-inquiry.com/episode/json/" + req.params.PatientID + "?Api-Key=" + apiKey;
				console.log(url);
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				request.get({url: url, headers: headers, proxy: "http://127.0.0.1:8888", maxAttempts: 5, retryDelay: 5000, retryStrategy: request.RetryStrategies.HTTPOrNetworkError}, function(err, response, body)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var episodes = JSON.parse(body);
					if (episodes.length == 0)
					{
						return callback(err, null);
					}
					
					epID = episodes[0].EpisodeId;
					console.log("Got Episode");
					setTimeout(function () 
					{
						return callback(null, apiKey, episodes[0].EpisodeId);
					}, 100);
				});

			},
			function(apiKey, episodeID, callback)
			{
				//Claim the episode, set an external ID (mock one, no need to store they can all be the same).
				console.log("claimed");
				var url = "http://devs.patient-inquiry.com/episode/json/" + "?Api-Key=" + apiKey;
				console.log(url);
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				request({url:url, method:'PUT', json: {episodeId: episodeID, externalEpisodeId: "0001"}, headers: headers, proxy: "http://127.0.0.1:8888", maxAttempts: 5, retryDelay: 5000, retryStrategy: request.RetryStrategies.HTTPOrNetworkError}, function (err, response, body) 
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					
					setTimeout(function () 
					{
						console.log(callback);
					  	return callback(null, apiKey);
					}, 100);
				});
			},
			function(apiKey, callback)
			{
				console.log("set visit");
				//Set Visit for 1 hour for setup(on this date).
				var url = "http://devs.patient-inquiry.com/visit2/json/?Api-Key=" + apiKey;
				var date = "\/Date({0}-0400)\/";
				date = date.replace("{0}", new Date().getTime());
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				request({url:url, method:'POST', json: {ClinicianType:"other", ExternalEpisodeId: "0001", MinutesSpent:60, VisitDate:date, Revenue:0}, headers:headers, proxy: "http://127.0.0.1:8888", maxAttempts: 5, retryDelay: 5000, retryStrategy: request.RetryStrategies.HTTPOrNetworkError}, function (err, response, body) 
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					
					setTimeout(function () 
					{
					  return callback(null, apiKey);
					}, 100);
				});
			},
			function (apiKey, callback)
			{
				console.log("get sess count");
				var sql = "SELECT COUNT(*) as SessionCount FROM Session WHERE PatientID = ?";
				var val = req.params.PatientID;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					
					setTimeout(function () 
					{
					  return callback(err, apiKey, results[0].SessionCount);
					}, 100);
				});
			},
			function(apiKey, sessionCount, callback)
			{
				console.log("discharge patient");
				//Discharge the patient.
				var url = "http://devs.patient-inquiry.com/discharge/json/" + "?Api-Key=" + apiKey;
				var time = new Date().getTime();
				var date = "\/Date({0}-0400)\/";
				date = date.replace("{0}", time);
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				
				request({url:url, method:'POST', json: {ExternalEpisodeId: "0001", LastVisit:date, InterruptionDays: 0, ClinicianComments: "NUMBER GOPHYSIO SESSIONS=" + sessionCount}, headers:headers, proxy: "http://127.0.0.1:8888", maxAttempts: 5, retryDelay: 5000, retryStrategy: request.RetryStrategies.HTTPOrNetworkError},
				function (err, response, body)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					setTimeout(function () 
					{
					  return callback(null, apiKey);
					}, 100);
				});
			},
			function(apiKey, callback)
			{
				console.log("API Key");
				//Get Report
				var url = "http://devs.patient-inquiry.com/report/json/" + epID + "/2?Api-Key=" + apiKey;
				var path = '/root/WebMedia/FOTOReports/' + req.params.PatientID + ".pdf";
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				

				request.get({url: url, headers: headers, proxy: "http://127.0.0.1:8888"}, 
				function (err, response, body)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					var byteArray = JSON.parse(body);
					var arr = Uint8Array.from(byteArray);

					fs.writeFile(path, arr, 'binary', function (err)
			    	{
			    		if (err)
						{
							console.log(err);
							return callback(err, null);
						}
			    		setTimeout(function () 
						{
						  return callback(null, apiKey);
						}, 100);
			    		
			    	});
					
				});
			},
			function (apiKey, callback)
			{
				var sql = "SELECT (DATEDIFF(NOW(), PatientRecordsView.AddedDate)) as Duration from PatientRecordsView WHERE PatientID = ?";
				var val = req.params.PatientID;
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					return callback(null, apiKey, results[0].Duration);
				});
			},
			function (apiKey, duration, callback)
			{
				//Generate Custom Report.
				console.log("Generating Custom Report");
				//Get Artifact data.
				var url = "http://devs.patient-inquiry.com/artifact2/json/episode/0001?Api-Key=" + apiKey;
				var headers = {'User-Agent': 'Fiddler', 'Content-Type' : 'text/json'};

				request.get({url: url, headers: headers, proxy: "http://127.0.0.1:8888", maxAttempts: 5, retryDelay: 5000, retryStrategy: request.RetryStrategies.HTTPOrNetworkError}, function(err, response, body)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					console.log("Got Artifacts");
					var artifacts = JSON.parse(body);
					//Get artifact for report type 2.
					var dischargeArtifact = artifacts.filter(function(item)
					{
						return (item.ReportType == 2 && item.EpisodeId == epID);
					})[0];

					var critera = dischargeArtifact.RiskAdjustedItems.map(function(item)
					{
						return {Key: item.Attribute, Value: item.Value};
					});

					var fsSummary = dischargeArtifact.FunctionalAssessments.map(function(item)
					{
						var array = [];
						array.push(item.Descriptor);
						item.FunctionalScoreSummaries.forEach(function(x)
						{
							//array.push({Key: x.Label, Value: x.FunctionalScore});
							array.push(x.FunctionalScore)
						});
						//array.push({Key: "Predicted", Value: item.PredictedChangeSummary.FunctionalScoreGoal});
						array.push(item.PredictedChangeSummary.FunctionalScoreGoal);
						return array;
					});

					fsSummary.push(['Risk Adjusted Statistical FOTO*', dischargeArtifact.FunctionalAssessments[0].RiskAdjustedStatisticalFOTO]);

					var fsTableHeader = [];
					fsTableHeader.push("Name");
					dischargeArtifact.FunctionalAssessments[0].FunctionalScoreSummaries.forEach(function(item)
					{
						fsTableHeader.push(item.Label);
					});
					fsTableHeader.push("Predicted");

					var additional = dischargeArtifact.FunctionalAssessments[0].AssessmentAttributes.map(function(item)
					{
						return {Key: item.Attribute, Predicted: item.Value, Final: ""};
					});

					//Add custom visits number
					additional.find(function(item)
					{
						return (item.Key == "Visits");
					}).Final = req.params.Visits;

					//Add Duration
					additional.find(function(item)
					{
						return (item.Key == "Duration");
					}).Final = duration;


					additional.push({Key: "Points Of Physical Change", Predicted: dischargeArtifact.FunctionalAssessments[0].PredictedChangeSummary.PredictedChange});

					//Log this data to ensure it's correct.
					var templateData = {Critera: critera, FSTableHeader: fsTableHeader, FSSummary: fsSummary, Additional: additional};
					console.log(templateData);

					//Sort out a template engine to convert this into HTML.
					var html = jade.renderFile('template.jade', templateData);
					var filePath = "/root/WebMedia/GoPhysioReports/" + req.params.PatientID + ".pdf";
					pdf.create(html).toFile(filePath, function(err, res) 
					{
					  	if (err)
						{
							console.log(err);
							return callback(err);
						}
					  	return callback(null);
					});
				});
				

				//Convert to PDF.
				//Callback with fotoreport and custom report. 
			},
			function (callback)
			{
				var sql = "UPDATE PatientRecords SET IsDischarged = TRUE, InDischargeProcess = FALSE WHERE PatientID = ?"
				var val = req.params.PatientID;
				console.log(callback);
				con.query(sql, val, function(err, results)
				{
					if (err)
					{
						console.log(err);
						return callback(err, null);
					}
					console.log(callback);
					return callback(null, null);
				});
			}
		],
		function(err, result)
		{
			//Return Success / Failure.
			if (err)
			{
				return next(err);
			}
			console.log("here");
			return res.status(200).send("success");
		}
	);
})

app.get('/discharge/dischargeReports/:OAuthToken/:PatientID', function(req, res, next)
{
	var sql = "SELECT PatientID, IsDischarged FROM PatientRecords WHERE PatientID = ?";
	var val = req.params.PatientID;
	console.log("discharge report requested");
	con.query(sql, val, function(err, results)
	{
		if (err)
		{
			return next(err);
		}
		var obj = 
		{
			IsDischarged: results[0].IsDischarged, 
			FOTOReportURL: "http://api.4dhealthscience.com:4000/FOTOReports/" + req.params.PatientID + ".pdf",
			GoPhysioReportURL: "http://api.4dhealthscience.com:4000/GoPhysioReports/" + req.params.PatientID + ".pdf"
		};

		return res.status(200).json(obj);
	});
})

app.use(function (err, req, res, next) 
{
	if (err)
	{
		console.log(err);
		res.status(err.statusCode || 500).json(err);
	}
});

app.listen(3000, function () 
{
  console.log('Example app listening on port 3000!')
})

