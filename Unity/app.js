const express = require('express')
const app = express()
const moment = require('moment');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var fs = require("fs");
var exec = require('child_process').exec;
var rmdir = require('rmdir');
var async = require('async');
var request = require('request');
var Validator = require('validatorjs');
var errors = require('throw.js');


var db_config = {
  host: "localhost",
  user: "admin",
  password: "ff1312d653e45756b87e3ceef5a774b87907be60ba7cee9e",
  database: "GoPhysio"
};

var con;

function handleDisconnect() 
{
  con = mysql.createConnection(db_config); // Recreate the connection, since the old one cannot be reused.

con.connect(function(err) 
{
  if(err) 
  {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
  }
});

con.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

app.use(bodyParser.json({limit: '100mb', type: 'application/json'}));

var jsonParser = bodyParser.json();

app.param('PatientID', function (req, res, next, patientID )
{
  //Check DB to see that user is in there.
  var sql = "SELECT COUNT(*) as Count FROM PatientRecords WHERE PatientID = ?";
  var val = patientID;
  con.query(sql, val, function(err, results)
  {
    //return OAuth ID or 400 error.
    if (results[0].Count == 1)
    {
      return next();
    }
    else
    {
      console.log('patient ' + patientID + ' not found');
      return next(new errors.NotFound());
    }
  });
})

app.get('/GetClientData/:PatientID', function (req, res, next) 
{
	//Get User's name, Exercise list, random goals.
  async.parallel
  (
    [
      function(callback)
      {
        var sql = "SELECT PatientRecords.PatientID, PatientRecords.PractitionerID, PractitionerRecords.OAuthPrimary, PractitionerRecords.FirstName AS PracFirst, PractitionerRecords.LastName AS PracLast, PatientRecords.FirstName AS PatFirst, PatientRecords.LastName as PatLast FROM PatientRecords INNER JOIN PractitionerRecords ON PatientRecords.PractitionerID = PractitionerRecords.OAuthPrimary WHERE PatientRecords.PatientID = ?";
        var val = req.params.PatientID;
        con.query(sql, val, function (err, results) 
        {
          return callback(null, {Key: "Details", Value: results[0]});
        });
      },

      function(callback)
      {
        var sql = "SELECT ID, Description FROM Goals WHERE Goals.PatientID = ?";
        var val = req.params.PatientID;
        con.query(sql, val, function (err, results) 
        {
          return callback(err, {Key:"Goals", Value: results});
        });
      },

      function (callback)
      {
        var sql = "SELECT ActiveExerciseView.ID as ActiveExerciseID, ActiveExerciseView.TargetReps, ActiveExerciseView.CompletedReps, ActiveExerciseView.Accuracy, Exercises.ID as ExerciseID, Exercises.VideoURL, Exercises.VideoURL, Exercises.Title, Exercises.Description, ExerciseUpdateView.UpdatedTime, ExerciseUpdateView.UpdatedSinceLastSession, LastExerciseView.Accuracy as LastAccuracy FROM ActiveExerciseView JOIN Exercises ON ActiveExerciseView.ExerciseID = Exercises.ID JOIN ExerciseUpdateView ON ExerciseUpdateView.ID = ActiveExerciseView.ID LEFT JOIN LastExerciseView ON LastExerciseView.ActiveExerciseID = ActiveExerciseView.ID WHERE ActiveExerciseView.PatientID = ?";
        var val = req.params.PatientID;
        con.query(sql, val, function (err, results) 
        {
          return callback(err, {Key: "Exercises", Value: results});
        });
      },
      function (callback)
      {
        var sql = "SELECT COUNT(*) as SessionCountToday FROM Session WHERE Session.PatientID = ? AND DATE(Session.DateOfSession) = DATE(NOW())";
        var val = req.params.PatientID;
        con.query(sql, val, function (err, results) 
        {
          return callback(err, {Key: "SessionCountToday", Value: results[0].SessionCountToday});
        });
      },
      function(callback)
      {
        var sql = "SELECT LastSessionAccuracy FROM LastSessionStats WHERE PatientID = ?";
        var val = req.params.PatientID;
        con.query(sql, val, function(err, results)
        {
          return callback(err, {Key: "LastSessionAccuracy", Value: results[0].LastSessionAccuracy});
        });
      }
  ],
    function (err, results)
    {
      if (err)
      {
        return next(err);
      }
      var obj = {};
      results.forEach(function(item)
      {
        obj[item.Key] = item.Value;
      });

      var obj = {PractitionerName: obj.Details.PracFirst + " " + obj.Details.PracLast, PatientName: obj.Details.PatFirst + " " + obj.Details.PatLast, Exercises: obj.Exercises, Goals: obj.Goals, SessionCountToday: obj.SessionCountToday, LastSessionAccuracy: obj.LastSessionAccuracy};
      return res.status(200).json(obj);
    }
  );
	
})

var alertSMS = "GOPHYSIO ALERT\n \n A Patient using gophysio has reported pain.  Please click the following link for immediate details. {$LINK}";

app.put('/JointPainReport/:PatientID', jsonParser, function (req, res, next) 
{
	if (!req.body) next(new errors.BadRequest());

  var validationRules = 
  {
    ExerciseID: 'required',
    JointPainData: 'required'
  };

  var validation = new Validator(req.body, validationRules);

  if (validation.fails())
  {
    return next(errors.BadRequest(validation.errors.all()));
  }

  console.log(req.body.SessionID);
	var post = {ActiveExerciseID: req.body.ExerciseID, SessionID: req.body.SessionID, PatientID: req.params.PatientID, JointPainData: JSON.stringify(req.body.JointPainData)};
	//Insert pain report Into Databaes.
	var sql = "INSERT INTO PainReports SET ?";
	con.query(sql, post, function (err, result) 
	{
    	if (err)
    	{
        return next(err);
    	}
    	res.send("Sucess");
    	var insertID = result.insertId;

    	//Get Patient Phone number.
    	var sql = "SELECT PatientRecords.PatientID, PatientRecords.FirstName as PatientFirstName, PatientRecords.LastName as PatientLastName, PractitionerRecords.FirstName as PractitonerFirstName, PractitionerRecords.ClinicPhone FROM PatientRecords JOIN PractitionerRecords ON PractitionerRecords.OAuthPrimary = PatientRecords.PractitionerID WHERE PatientRecords.PatientID = ?";
    	var val = req.params.PatientID;
    	con.query(sql, val, function(err, results)
    	{
        var encodeParam = encodeURI("patient-pain.html?patientID=" + req.params.PatientID + '&' + "painReportID=" + insertID);
    		var link = "http://4dhealthscience.com/gophysio/office/v3/login.html?forcedlogin=true&reurl=" + encodeParam;
    		//Generate SMS Text.
    		var sms = alertSMS.replace("{$LINK}", link)
    		//Send SMS
	    	request({
          method: 'POST',
          url: 'http://textbelt.com/text',
          proxy: "http://127.0.0.1:8888",
			  form: {
			    phone: results[0].ClinicPhone,
			    message: sms,
			    key: '356a8b637ef98c0e9fe1d802103e4b0747673eff3SSgdt0uIzzR2q6fQoe0bTlGB' 
			  },
			}, function(err, httpResponse, body) 
        {
          console.log("sms");
          console.log(err);
          console.log(body);
  			})
    	});

    	//Create a temp folder with joint pain report ID.
    	var dir = './' + insertID
    	fs.mkdirSync(dir);
    	console.log(" number of videos " + req.body.VideoImages.length);

    	//Write all images into this folder, named 1,2,3,4 ect.
    	async.forEachOf(req.body.VideoImages, function(value, key, callback)
    	{
    		var imagePath = dir + '/' + key + '.jpeg';
    		 fs.writeFile(imagePath, value, 'base64', function (err)
    		 {
    		 	callback();
    		 });
    	},
	    	function (err)
	    	{
	    		//Call FFMPEG on this folder.
	    		
	    		var path = "/root/WebMedia/PainVideo/";
	    		var command = "ffmpeg -r 5 -f image2 -s 640x480 -i " + dir + "/" + "%d.jpeg -vcodec libx264 -crf 25  -pix_fmt yuv420p " + path + insertID + ".mp4";
	    		exec(command, function (err, stdout, stderr) 
	    		{
 					// your callback goes here
 					if (stderr || err)
 					{
 						console.log(stderr);
 						console.log(err);
 					}
 					//Delete Folder and contents.
 					rmdir(dir, function(err, dirs, files)
 					{
 					});

				});
	    	}
    	);
  	});
	//Return success or fail. (send)
})


app.put('/GoalResponse/:PatientID', jsonParser, function(req, res, next)
{
	if (!req.body) next(new errors.BadRequest())

  var validationRules = 
  {
    GoalID: 'required',
    Response: 'required'
  };

  var validation = new Validator(req.body, validationRules);

  if (validation.fails())
  {
    return next(errors.BadRequest(validation.errors.all()));
  }

	var post = {GoalID: req.body.GoalID, Response: req.body.Response};
	//Insert pain report Into Databaes.
	var sql = "INSERT INTO GoalResponses SET ?";
	con.query(sql, post, function (err, result) 
	{
    	if (err)
    	{
        return next(err);
    	} 
    	return res.status(200).send('success');
  	});
	//Return success or fail. (send)
});

app.put('/SessionData/:PatientID', jsonParser, function(req, res, next)
{
	if (!req.body) next(new errors.BadRequest());

  var validationRules = 
  {
    DateOfSession: 'required',
    UserAccuracies: 'required'
  };

  var validation = new Validator(req.body, validationRules);

  if (validation.fails())
  {
    return next(errors.BadRequest(validation.errors.all()));
  }

	var date = moment(req.body.DateOfSession).format('YYYY-MM-DD HH:mm:ss');
	var post = {PatientID: req.params.PatientID, DateOfSession: date};
	//Insert pain report Into Databaes.
	var sql = "INSERT INTO Session SET ?";
	con.query(sql, post, function (err, result) 
	{
    	if (err)
    	{
        return next(err);
    	}
      var insertID = result.insertId;

			var folderPath = '/root/WebMedia/SessionImages/';
	    	var imageName = insertID + '.jpeg';
	    	var imagePath = folderPath + imageName;
	    	//var buff = new Buffer(req.body.UserImage, 'base64');
	    	fs.writeFile(imagePath, req.body.SessionImage, 'base64', function (err)
	    	{
	    		if (err) return next(err);
	    	});


    	sql = "INSERT INTO SessionExerciseRep(SessionID, ActiveExerciseID, Accuracy, CompletedDate) VALUES ?";
    	post = req.body.UserAccuracies.map(function(item)
    	{
    		var date = moment(item.CompletedDate).format('YYYY-MM-DD HH:mm:ss');
    		return [insertID, item.ID, item.Accuracy, date]
    	});
    	con.query(sql, [post], function(err, result)
    	{
    		if (err)
    		{
          return next(err);
    		}
        console.log("before send");
    		return res.json({SessionID: insertID})
    	});
  	});
	//Return success or fail. (send)
})

app.put('/InventoryCheckout/:PatientID', jsonParser, function(req, res, next)
{
    if (!req.body) next(new errors.BadRequest());

    var validationRules = 
    {
      ID: 'required',
    };
    var validation = new Validator(req.body, validationRules);

    if (validation.fails())
    {
      return next(errors.BadRequest(validation.errors.all()));
    }

    //Update MAC's patient. 

    var sql = "UPDATE AndroidDevices SET PatientID = ? WHERE ID = ?";
    var val = [req.params.PatientID, req.body.ID];
    con.query(sql, val, function(err, results)
    {
      if (err)
      {
        return next(err);
      }
      return res.status(200).send('success');
    });
});

app.put('/Combo/:PatientID', jsonParser, function(req, res, next)
{
    if (!req.body) next(new errors.BadRequest())

    var validationRules = 
    {
      ExerciseData: 'required',
      JointPainData: 'required'
    };

    var validation = new Validator(req.body, validationRules);

    if (validation.fails())
    {
      return next(errors.BadRequest(validation.errors.all()));
    }

    async.waterfall
    (
      [
        function(callback)
        {
          //Call self.exercises, get returned id.
          var path = "/SessionData/" + req.params.PatientID;
          var url = "http://127.0.0.1:8080" + path;
          request
          ({
            method: 'PUT',
            url: url,
            json: req.body.ExerciseData
          },
          function(err, httpResponse, body) 
          {
            if (err) return callback(err);
            if (httpResponse.statusCode != 200) return callback(new errors.BadRequest(body));
            return callback(null, body.SessionID);
          });
        },
        function (sessionID, callback)
        {
          req.body.JointPainData.SessionID = sessionID;
          //Call self.exercises, get returned id.
          var path = "/JointPainReport/" + req.params.PatientID;
          var url = "http://127.0.0.1:8080" + path;
          request
          ({
            method: 'PUT',
            url: url,
            json: req.body.JointPainData, 
          },
          function(err, httpResponse, body) 
          {
            if (err) return callback(err);
            console.log(body);
            if (httpResponse.statusCode != 200) return callback(new errors.BadRequest(body));
            return callback(null, null);
          });
        }
      ],
      function(err, results)
      {
        if (err) return next(err);

        res.send("Sucess");
        console.log("returned");
      }
    );
});

app.put('/TrackCachedUsage/:PatientID', jsonParser, function(req, res, next)
{
  if (!req.body) next(new errors.BadRequest())

  var validationRules = 
  {
    CachedCount: 'required',  
  };

  var validation = new Validator(req.body, validationRules);

  if (validation.fails())
  {
    return next(errors.BadRequest(validation.errors.all()));
  }

  var sql = "INSERT INTO CachedDataUsageSession (PatientID, CachedCallCount) VALUES(?, ?)";
  var val = [req.params.PatientID, req.body.CachedCount];
  con.query(sql, val, function(err, result)
  {
    if (err) return next(err);
    return res.send("success");
  });
});



app.use(function (err, req, res, next) 
{
  if (err)
  {
    console.log(err);
    return res.status(err.statusCode || 500).json(err);
  }
});

app.listen(8080, function () 
{
  console.log('Example app listening on port 3000!')
})
