var repeat = require('repeat');
var mysql = require('mysql');
var fs = require('fs');

var con = mysql.createConnection({
  host: "localhost",
  user: "admin",
  password: "ff1312d653e45756b87e3ceef5a774b87907be60ba7cee9e",
  database: "GoPhysio"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});


function deleteFiles()
{
	console.log("deleteFiles");
	//Query table to get all outstanding deletes. (order by insert ID desc)
	var sql = "SELECT Path FROM FileDeleteQueue WHERE Deleted = FALSE";
	con.query(sql, function(err, results)
	{
		if (err) return console.log(err);

		//foreach result -> Delete file.
		results.forEach(function(item)
		{
			console.log(item.Path + " being deleted");
			fs.unlink(item.Path, function(err)
			{
				if (err)
				{
					return console.log(item.Path + "___" + err);
				}
			});
		});
		
		//Remove all entries from table.
		var sql = "UPDATE FileDeleteQueue SET Deleted = TRUE";
		con.query(sql, function(err, result)
		{
			if (err) return console.log(err);
		});
	});
}

//Repeat once per day.
repeat(() => deleteFiles()).every(1440, 'minutes').start();